﻿using System;
using System.Collections.Generic;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Booking : BaseEntity
	{
		public DateTime BookingDateTime { get; set; }
		public double Deposit { get; set; }
		public string Description { get; set; }
		public DateTime CreatedDateTime { get; set; }
		public bool IsOverdue { get; set; }

		// virtual for navigation
		public virtual ICollection<Job> Jobs { get; set; }

		public Booking()
		{
			Jobs = new HashSet<Job>();
		}
	}
}
