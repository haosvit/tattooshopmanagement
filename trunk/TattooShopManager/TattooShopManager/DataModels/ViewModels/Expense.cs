﻿using System;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Expense : BaseEntity
	{
		public Guid ShopId { get; set; }
		public Guid EmployeeId { get; set; }
		public double Amount { get; set; }
		public string Description { get; set; }
		public DateTime CreatedDateTime { get; set; }

		public virtual Shop Shop { get; set; }
		public virtual Employee Employee { get; set; }
	}
}
