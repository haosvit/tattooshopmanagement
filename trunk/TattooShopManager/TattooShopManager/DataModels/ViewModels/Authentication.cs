﻿using System;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Authentication : BaseEntity
	{
		public Guid EmployeeId { get; set; }
		public string Password { get; set; }

		public virtual Employee Employee { get; set; }
	}
}
