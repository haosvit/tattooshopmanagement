﻿using System.Collections.Generic;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Shop : BaseEntity
	{
		public string Name { get; set; }
		public string Address { get; set; }

		public ICollection<Employee> Employees { get; set; }
		public ICollection<Expense> Expenses { get; set; }
		public ICollection<Job> Jobs { get; set; }
	}
}
