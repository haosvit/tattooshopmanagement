﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TattooShopManager.DataModels.ViewModels
{
	public class BaseEntity
	{
		[Key]
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
	}
}
