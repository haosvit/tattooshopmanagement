﻿using System;
using System.Collections.Generic;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Job : BaseEntity
	{
		public Guid ShopId { get; set; }
		public Guid AdminId { get; set; }
		public Guid ArtistId { get; set; }
		public Guid CustomerId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public double Price { get; set; }
		public double ArtistSalaryFactor { get; set; }
		public double TotalPaid { get; set; }

		public virtual Shop Shop { get; set; }
		public virtual Employee Admin { get; set; }
		public virtual Employee Artist { get; set; }
		public virtual Customer Customer { get; set; }

		public ICollection<PayCheck> PayChecks { get; set; }
		public ICollection<ImagePath> ImagePaths { get; set; }
		
		// virtual for navigation
		public virtual ICollection<Booking> Bookings { get; set; }

		public Job()
		{
			Bookings = new HashSet<Booking>();
		}
	}
}
