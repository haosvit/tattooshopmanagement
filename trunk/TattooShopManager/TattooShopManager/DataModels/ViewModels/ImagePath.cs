﻿using System;

namespace TattooShopManager.DataModels.ViewModels
{
	public class ImagePath : BaseEntity
	{
		public Guid JobId { get; set; }
		public string Path { get; set; }

		public virtual Job Job { get; set; }
	}
}
