﻿using System;

namespace TattooShopManager.DataModels.ViewModels
{
	public class SalaryFactor : BaseEntity
	{
		public Guid	EmployeeId { get; set; }
		public float Factor { get; set; }
		public DateTime CreatedDateTime { get; set; }
	}
}
