﻿using System.Collections.Generic;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Customer : BaseEntity
	{
		public string FullName { get; set; }
		public bool IsMale { get; set; }
		public string PhoneNumber { get; set; }

		public ICollection<Job> Jobs { get; set; }
	}
}
