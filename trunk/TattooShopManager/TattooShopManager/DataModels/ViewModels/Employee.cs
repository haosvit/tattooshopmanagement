﻿using System;
using System.Collections.Generic;
using TattooShopManager.DataModels.Types;

namespace TattooShopManager.DataModels.ViewModels
{
	public class Employee : BaseEntity
	{
		public Guid	ShopId { get; set; }
		public string FullName { get; set; }
		public bool IsMale { get; set; }
		public DateTime? DoB { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public Role Role { get; set; }

		public virtual Shop Shop { get; set; }
		public ICollection<SalaryFactor> SalaryFactors { get; set; }
		// jobs that admin manages
		public virtual ICollection<Job> AdminJobs { get; set; }
		// jobs that artist performs
		public virtual ICollection<Job> ArtistJobs { get; set; }
	}
}
 