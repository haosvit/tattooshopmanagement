﻿using System;

namespace TattooShopManager.DataModels.ViewModels
{
	public class PayCheck : BaseEntity
	{
		public Guid JobId { get; set; }
		public double Amount { get; set; }
		public DateTime CreatedDateTime { get; set; }

		public virtual Job Job { get; set; }
	}
}
