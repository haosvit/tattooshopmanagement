﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace TattooShopManager.GUIs
{
	public partial class AddEventToCalendar : Form
	{
		static string[] Scopes = { CalendarService.Scope.Calendar };
		static string ApplicationName = "Tattoo Shop";

		private CalendarList _calendars;

		public AddEventToCalendar()
		{
			InitializeComponent();
			var credential = AuthorizeCalendar();
			//RequestEventList(credential);
			RetrieveCalendarList(credential);
		}

		public UserCredential AuthorizeCalendar()
		{
			UserCredential credential;

			using (var stream =
				new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
			{
				string credPath = Environment.GetFolderPath(
					Environment.SpecialFolder.Personal);
				credPath = Path.Combine(credPath, ".credentials/tattooshop.json");

				credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					Scopes,
					"user",
					CancellationToken.None,
					new FileDataStore(credPath, true)).Result;
				Console.WriteLine("Credential file saved to: " + credPath);
				return credential;
			}
		}

		public void RequestEventList(UserCredential credential)
		{
			// Create Google Calendar API service.
			var service = new CalendarService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = ApplicationName,
			});

			// Define parameters of request.
			EventsResource.ListRequest request = service.Events.List("primary");
			request.TimeMin = DateTime.Now;
			request.ShowDeleted = false;
			request.SingleEvents = true;
			request.MaxResults = 10;
			request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

			// List events.
			Events events = request.Execute();
			Console.WriteLine("Upcoming events:");
			if (events.Items != null && events.Items.Count > 0)
			{
				foreach (var eventItem in events.Items)
				{
					string when = eventItem.Start.DateTime.ToString();
					if (String.IsNullOrEmpty(when))
					{
						when = eventItem.Start.Date;
					}
					Console.WriteLine("{0} ({1})", eventItem.Summary, when);
				}
			}
			else
			{
				Console.WriteLine("No upcoming events found.");
			}
			Console.Read();
		}

		private void RetrieveCalendarList(UserCredential credential)
		{
			var service = new CalendarService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = ApplicationName,
			});

			//build request
			CalendarListResource.ListRequest clistRequest = service.CalendarList.List();

			_calendars = clistRequest.Execute();

			cbbTargetCalendar.Items.AddRange(_calendars.Items.Select(i => i.Summary).ToArray());

			var primaryCalendar = _calendars.Items.FirstOrDefault(i => i.Primary ?? false)?.Summary;
			cbbTargetCalendar.Text = primaryCalendar;
		}

		private void btnAddEvent_Click(object sender, EventArgs e)
		{
			var service = new CalendarService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = AuthorizeCalendar(),
				ApplicationName = ApplicationName,
			});

			var startDate = new EventDateTime
			{
				Date = clStart.SelectionStart.ToString("yyyy-MM-dd")
			};

			var endDate = new EventDateTime
			{
				Date = clStart.SelectionStart.ToString("yyyy-MM-dd")
			};

			var eventBody = new Event()
			{
				Summary = tbEventSumary.Text,
				Start = startDate,
				End = startDate
			};

			var calendarId = _calendars.Items.FirstOrDefault(i => i.Summary == cbbTargetCalendar.Text)?.Id;
			var addEventRequest = service.Events.Insert(eventBody, calendarId);

			addEventRequest.Execute();
		}
	}
}
