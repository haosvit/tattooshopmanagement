﻿namespace TattooShopManager.GUIs
{
	partial class AddEventToCalendar
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnAddEvent = new System.Windows.Forms.Button();
			this.tbEventSumary = new System.Windows.Forms.TextBox();
			this.cbbTargetCalendar = new System.Windows.Forms.ComboBox();
			this.clStart = new System.Windows.Forms.MonthCalendar();
			this.clEnd = new System.Windows.Forms.MonthCalendar();
			this.SuspendLayout();
			// 
			// btnAddEvent
			// 
			this.btnAddEvent.Location = new System.Drawing.Point(12, 245);
			this.btnAddEvent.Name = "btnAddEvent";
			this.btnAddEvent.Size = new System.Drawing.Size(75, 23);
			this.btnAddEvent.TabIndex = 0;
			this.btnAddEvent.Text = "Add Event";
			this.btnAddEvent.UseVisualStyleBackColor = true;
			this.btnAddEvent.Click += new System.EventHandler(this.btnAddEvent_Click);
			// 
			// tbEventSumary
			// 
			this.tbEventSumary.Location = new System.Drawing.Point(12, 12);
			this.tbEventSumary.Name = "tbEventSumary";
			this.tbEventSumary.Size = new System.Drawing.Size(469, 20);
			this.tbEventSumary.TabIndex = 1;
			// 
			// cbbTargetCalendar
			// 
			this.cbbTargetCalendar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbbTargetCalendar.FormattingEnabled = true;
			this.cbbTargetCalendar.Location = new System.Drawing.Point(12, 218);
			this.cbbTargetCalendar.Name = "cbbTargetCalendar";
			this.cbbTargetCalendar.Size = new System.Drawing.Size(227, 21);
			this.cbbTargetCalendar.TabIndex = 2;
			// 
			// clStart
			// 
			this.clStart.Location = new System.Drawing.Point(12, 44);
			this.clStart.Name = "clStart";
			this.clStart.TabIndex = 3;
			// 
			// clEnd
			// 
			this.clEnd.Location = new System.Drawing.Point(254, 44);
			this.clEnd.Name = "clEnd";
			this.clEnd.TabIndex = 4;
			// 
			// AddEventToCalendar
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(544, 366);
			this.Controls.Add(this.clEnd);
			this.Controls.Add(this.clStart);
			this.Controls.Add(this.cbbTargetCalendar);
			this.Controls.Add(this.tbEventSumary);
			this.Controls.Add(this.btnAddEvent);
			this.Name = "AddEventToCalendar";
			this.Text = "AddEventToCalendar";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnAddEvent;
		private System.Windows.Forms.TextBox tbEventSumary;
		private System.Windows.Forms.ComboBox cbbTargetCalendar;
		private System.Windows.Forms.MonthCalendar clStart;
		private System.Windows.Forms.MonthCalendar clEnd;
	}
}