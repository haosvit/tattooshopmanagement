﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using TattooShopManager.DBAccess;

namespace TattooShopManager.GUIs
{
	public partial class Splash : Form
	{
		public Splash()
		{
			InitializeComponent();
		}

		private void Splash_Load(object sender, System.EventArgs e)
		{
			bgwDatabaseInitialer.DoWork += InitDatabase;
			bgwDatabaseInitialer.RunWorkerCompleted += OnInitDbFinished;
			bgwDatabaseInitialer.RunWorkerAsync();
		}

		private void OnInitDbFinished(object sender, RunWorkerCompletedEventArgs e)
		{
			Hide();
			new AddEventToCalendar().Show();
		}

		private void InitDatabase(object sender, DoWorkEventArgs e)
		{
			TSDataContext tSDataContext = new TSDataContext();
			tSDataContext.InitDatabaseFirstTime();
		}
	}
}
