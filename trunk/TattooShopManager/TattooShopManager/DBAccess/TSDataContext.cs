﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using TattooShopManager.DataModels.ViewModels;

namespace TattooShopManager.DBAccess
{
	public class TSDataContext : DbContext
	{
		private static string _dbName = "TATTOO_SHOP_DB";

		// DbSets
		public DbSet<Shop> Shops { get; set; }
		public DbSet<Employee> Employees { get; set; }
		public DbSet<SalaryFactor> SalaryFactors { get; set; }
		public DbSet<Authentication> Authentications { get; set; }
		public DbSet<Expense> Expenses { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Job> Jobs { get; set; }
		public DbSet<Booking> Bookings { get; set; }
		public DbSet<PayCheck> PayChecks { get; set; }
		public DbSet<ImagePath> ImagePaths { get; set; }

		public TSDataContext() : base(_dbName)
		{
		}

		public void InitDatabaseFirstTime()
		{
			try
			{
				Database.SetInitializer(new DbInitializer());
				Database.Initialize(force: false);
				Database.CreateIfNotExists();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.InnerException);
			}
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			try
			{
				modelBuilder.Entity<Job>()
				.HasRequired(job => job.Admin)
				.WithMany(emp => emp.AdminJobs)
				.HasForeignKey(job => job.AdminId)
				.WillCascadeOnDelete(false);

				modelBuilder.Entity<Job>()
					.HasRequired(job => job.Artist)
					.WithMany(emp => emp.ArtistJobs)
					.HasForeignKey(job => job.ArtistId)
					.WillCascadeOnDelete(false);

				modelBuilder.Entity<Expense>()
					.HasRequired(exp => exp.Shop)
					.WithMany(shop => shop.Expenses)
					.HasForeignKey(exp => exp.ShopId)
					.WillCascadeOnDelete(false);

				base.OnModelCreating(modelBuilder);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.InnerException);
			}		
		}

		public class DbInitializer : DropCreateDatabaseIfModelChanges<TSDataContext>
		{
			protected override void Seed(TSDataContext context)
			{
				InitDatabase();
				base.Seed(context);
			}
		}

		public static void InitDatabase()
		{
			try
			{
				TSDataContext context = new TSDataContext();
				// Seed Shop
				Guid shopId;

				context.Shops.Add(new Shop
				{
					Id = shopId = Guid.NewGuid(),
					Name = "Default"
				});

				// Seed users
				Guid employeeId;
				context.Employees.Add(new Employee
				{
					Id = employeeId = Guid.NewGuid(),
					ShopId = shopId,
					FullName = "Admin",
					Role = DataModels.Types.Role.Admin
				});

				// default authentication
				context.Authentications.Add(new Authentication
				{
					Id = Guid.NewGuid(),
					EmployeeId = employeeId,
					Password = "admin"
				});

				context.SaveChanges();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.InnerException.Message);
			}

		}

	}
}
